# This Python file uses the following encoding: utf-8
import os
from pathlib import Path
import sys

from PySide6.QtGui import QPixmap, QImage
from PySide6.QtWidgets import QApplication, QWidget
from PySide6.QtCore import QFile
from PySide6.QtUiTools import QUiLoader
import cv2
from cv2 import VideoCapture
import numpy as np


class Widget(QWidget):
    def __init__(self):
        super(Widget, self).__init__()
        self.load_ui()

        self.video_path = None
        self.result_path = "D:/"

        self.widgets.chooseFieldButton.clicked.connect(self.choose_field)
        self.widgets.confirmButton.clicked.connect(self.confirm_field)
        self.widgets.handlePushButton.clicked.connect(self.handle_video)

    def load_ui(self):
        loader = QUiLoader()
        path = os.fspath(Path(__file__).resolve().parent / "form.ui")
        ui_file = QFile(path)
        ui_file.open(QFile.ReadOnly)
        self.widgets = loader.load(ui_file, self)
        ui_file.close()

    def choose_field(self):
        self.video_path = self.widgets.videoPathLineEdit.text()
        self.cap = VideoCapture(self.video_path)

        ret, frame = self.cap.read()

        frame = cv2.cvtColor(frame.copy(), cv2.COLOR_BGR2RGB)
        self.first_frame = frame

        pixmap = self.convert_image_to_pixmap(frame, 640, 360)
        self.widgets.firstFrameLabel.setPixmap(pixmap)

    def confirm_field(self):
        self.image_field = [self.image_rect_start[0], self.image_rect_start[1], self.image_rect_end[0], self.image_rect_end[1]]
        print(self.image_field)

    def handle_video(self):
        if self.widgets.resultPathLineEdit.text():
            self.result_path = self.widgets.resultPathLineEdit.text()

        frame_count = self.cap.get(cv2.CAP_PROP_FRAME_COUNT)
        frame_counter = 1

        self.widgets.handleVideoProgressBar.setValue(int(frame_counter / frame_count * 100))

        wrote_counter = 1
        last_image = self.first_frame[
                         self.image_field[1]:self.image_field[3],
                         self.image_field[0]:self.image_field[2]
                         ]

        frame_len = last_image.shape[0] * last_image.shape[1]
        self.write_image(last_image, wrote_counter)

        pixmap = self.convert_image_to_pixmap(last_image, 640, 360)
        self.widgets.firstFrameLabel.setPixmap(pixmap)

        last_image = cv2.cvtColor(last_image, cv2.COLOR_RGB2GRAY)

        frame_count = self.cap.get(cv2.CAP_PROP_FRAME_COUNT)
        frame_per_second = int(self.cap.get(cv2.CAP_PROP_FPS))

        while self.cap.isOpened():
            ret, frame = self.cap.read()

            if not ret:
                break

            if frame_counter % frame_per_second == 0:
                frame = frame[
                         self.image_field[1]:self.image_field[3],
                         self.image_field[0]:self.image_field[2]
                         ]

                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                image_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
                reshaped_last_image = last_image.reshape(-1)
                reshaped_image = image_gray.reshape(-1)
                diff_pixels = np.sum(reshaped_last_image != reshaped_image)
                diff_coef = diff_pixels / frame_len

                if diff_coef > 0.85:
                    pixmap = self.convert_image_to_pixmap(frame, 640, 360)
                    self.widgets.firstFrameLabel.setPixmap(pixmap)
                    wrote_counter += 1
                    self.write_image(frame, wrote_counter)
                    print(wrote_counter, diff_coef)
                    last_image = image_gray

            frame_counter += 1
            self.widgets.handleVideoProgressBar.setValue(int(frame_counter / frame_count * 100))

        self.widgets.handleVideoProgressBar.setValue(100)
        self.cap.release()

    def write_image(self, image, frame_number):
        path = os.path.abspath(self.result_path)
        image_path = os.path.join(path, "frame_{}.jpg".format(frame_number))
        cv2.imwrite(image_path, image)

    def mousePressEvent(self, e):
        loc = e.localPos()
        x, y = loc.x(), loc.y()
        label_x, label_y = self.widgets.firstFrameLabel.x(), self.widgets.firstFrameLabel.y()
        image_x, image_y = int((x - label_x) * 3), int((y - label_y) * 3)
        self.image_rect_start = (image_x, image_y)
        self.image_rect_end = (image_x, image_y)

        frame = self.first_frame.copy()
        frame = cv2.circle(frame, (image_x, image_y), 40, (255, 0, 0), 5)
        frame = cv2.rectangle(frame, self.image_rect_start, self.image_rect_end, (255, 0, 0), 4)

        pixmap = self.convert_image_to_pixmap(frame, 640, 360)
        self.widgets.firstFrameLabel.setPixmap(pixmap)

    def mouseMoveEvent(self, e):
        loc = e.localPos()
        x, y = loc.x(), loc.y()
        label_x, label_y = self.widgets.firstFrameLabel.x(), self.widgets.firstFrameLabel.y()
        image_x, image_y = int((x - label_x) * 3), int((y - label_y) * 3)
        self.image_rect_end = (image_x, image_y)
        frame = self.first_frame.copy()
        frame = cv2.rectangle(frame, self.image_rect_start, self.image_rect_end, (255, 0, 0), 4)
        pixmap = self.convert_image_to_pixmap(frame, 640, 360)
        self.widgets.firstFrameLabel.setPixmap(pixmap)

    def convert_image_to_pixmap(self, image, scale_x, scale_y):
        image = np.ascontiguousarray(image.copy())
        h, w, ch = image.shape
        bytesPerLine = ch * w
        convertToQtFormat = QImage(image.data, w, h, bytesPerLine, QImage.Format_RGB888)
        image = convertToQtFormat.scaled(scale_x, scale_y)
        return QPixmap.fromImage(image)

if __name__ == "__main__":
    app = QApplication([])
    widget = Widget()
    widget.show()
    sys.exit(app.exec_())
